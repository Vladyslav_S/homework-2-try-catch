"use stirct";

/* Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch. 
Используем, когда ожидаем вводные данные от пользователя и в них часто будут ошибки */

function listToHTML(array, path) {
  const ul = document.createElement("ul");
  const propertyList = ["author", "name", "price"];

  array.forEach((object) => {
    const li = document.createElement("li");
    let counter = 0;

    propertyList.forEach((property) => {
      try {
        if (!Object.keys(object).includes(property)) {
          throw new Error(`${property} not exist!`);
        } else {
          counter++;
        }
      } catch (error) {
        console.log(error.message);
      }
    });
    if (counter === propertyList.length) {
      Object.values(object).forEach((el) => {
        el += " ";
        li.append(el);
      });
      ul.append(li);
    }
  });
  path.append(ul);
}

const root = document.getElementById("root");
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

listToHTML(books, root);
